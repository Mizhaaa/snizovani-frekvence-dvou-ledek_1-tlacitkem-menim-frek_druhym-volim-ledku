/* vzorový projekt (sem pište stručný popis projektu)

*/

#include "stm8s.h"
#include "milis.h"


void main(void){
	
uint16_t last_time1 = 0;
uint16_t last_time2 = 0;

uint16_t frekvence1 = 250;
uint16_t frekvence2 = 250;

uint8_t volba_tlacitka = 0;


CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // 16MHz z interního RC oscilátoru
init_milis(); 

//GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
//GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_IT);


GPIO_Init(GPIOD, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_SLOW); //LED 1
GPIO_Init(GPIOD, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW); //LED 2

GPIO_Init(GPIOG, GPIO_PIN_2, GPIO_MODE_IN_FL_NO_IT);//Tlacitko 1
GPIO_Init(GPIOE, GPIO_PIN_4, GPIO_MODE_IN_FL_NO_IT);//Tlacitko 2


  while (1){
		
		
		//frekvence blikanio pomoci milisu
		 if (milis()-last_time1>frekvence1)
			{
				GPIO_WriteReverse(GPIOD,GPIO_PIN_6);
				last_time1=milis();
			}
		
		 if (milis()-last_time2>frekvence2)
			{
				GPIO_WriteReverse(GPIOD,GPIO_PIN_5);
				last_time2=milis();
			}
			
			
			//funkce tlacitek
			
			
			//volba frekvence
	
		if(GPIO_ReadInputPin(GPIOG,GPIO_PIN_2)==RESET) // kontrolujeme zda je tlačítko1 stisknuté... (je na PE4 log.0)	
		{ 
			if(volba_tlacitka == 0)
			{
				frekvence1+=250;
				while(GPIO_ReadInputPin(GPIOG,GPIO_PIN_2)==RESET);
				if(frekvence1>3000)
				{
					frekvence1=250;
				}
			}
			if(volba_tlacitka == 1)
			{
				frekvence2+=250;
				while(GPIO_ReadInputPin(GPIOG,GPIO_PIN_2)==RESET);
				if(frekvence2>3000)
				{
					frekvence2=250;
				}

			}
		}
		
		
		//volba ledky
		if(GPIO_ReadInputPin(GPIOE,GPIO_PIN_4)==RESET) // kontrolujeme zda je tlačítko2 stisknuté... (je na PE4 log.0)
		{ 
		
			volba_tlacitka+=1;
			while(GPIO_ReadInputPin(GPIOE,GPIO_PIN_4)==RESET)
			if (volba_tlacitka > 1)
			{
				volba_tlacitka = 0;
			}
		}
		
	
	
		
		
		

  }
}


// pod tímto komentářem nic neměňte 
#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval : None
  */
void assert_failed(u8* file, u32 line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
